#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

int main() {
    int table[8][8]={{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0}};
    int n,x,y,out=64;
    scanf("%d",&n);
    for(int i=0;i<n;i++){
        scanf("%d %d",&x,&y);
        for(int j=0;j<8;j++){
            if(table[j][y-1]==0){
                table[j][y-1]++;
                out--;
            }
        }
        for(int j=0;j<8;j++){
            if(table[x-1][j]==0){
                table[x-1][j]++;
                out--;
            }
        }
    }
    printf("%d",out);
}
